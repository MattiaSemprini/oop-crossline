﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OOP_crossline.drawable.factory;
using OOP_crossline.src.actor.player;
using OOP_crossline.src.actor.robot;
using OOP_crossline.drawable.product;
using OOP_crossline.drawable;
using System;

namespace OOP_crosslineTest
{
    [TestClass]
    public class UnitTest1
    {
        //Initialize two variables with two different types of animations

        ISkinProduct a = new SkinFactoryEntityImpl<IPlayer>().createSkin(new PlayerImpl());
        ISkinProduct b = new SkinFactoryEntityImpl<IPlayer>().createSkinWithState(new PlayerImpl(), OOP_crossline.drawable.AnimationState.State.IS_JUMPING);


        //Make sure that the types of animations correspond to the default value
        [TestMethod]
        public void TestCreateSkin()
        {

            Assert.AreEqual(AnimationState.State.IS_IDLE, a.GetState());

            Assert.AreNotEqual(AnimationState.State.IS_JUMPING, a.GetState());

        }

        //Check if animation with state is correct
        [TestMethod]
        public void TestCreateSkinWithState()
            {
                
                Assert.AreEqual(AnimationState.State.IS_JUMPING, b.GetState());

                Assert.AreNotEqual(AnimationState.State.IS_IDLE, b.GetState());

            }

        //Control that the idle robot is created correctly
        [TestMethod]
        public void TestCreateSkinRobot()
        {
            Assert.ThrowsException<NotImplementedException>( () => new SkinFactoryEntityImpl<IRobot>().createSkin(new RobotImpl() ) );
        }
    }
}
