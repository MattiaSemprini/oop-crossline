﻿using OOP_crossline.drawable.product;
using OOP_crossline.drawable.product.attributes.idle;
using OOP_crossline.drawable.product.attributes.jump;
using OOP_crossline.src.actor.robot;
using OOP_crossline.src.actor.player;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable.factory
{
    public class SkinFactoryEntityImpl<X> : ISkinFactory<X>
    {
        private ISkinProduct animated;
        private SkinStrategy<X> skinStrategy;
        public ISkinProduct createSkin(X entity)
        {


            /*
             * Determine what skin would be constructed by class reference.
             */
            if (entity is IPlayer) {


                this.skinStrategy = new SkinStrategy<X>(new IdlePlayerImpl(),
                        new JumpPlayerImpl());
                this.animated = skinStrategy.idle();

            } else if (entity is IRobot) {


                this.skinStrategy = new SkinStrategy<X>(new IdleRobotImpl(),
                      new JumpRobotImpl());
                this.animated = skinStrategy.idle();

            } else {
                Console.WriteLine("Is not possible to draw this entity" + entity);
            }
            return this.animated;
        }

    public ISkinProduct createSkinWithState(X entity, AnimationState.State a)
        {

            if (entity is IPlayer) {


                this.skinStrategy = new SkinStrategy<X>(new IdlePlayerImpl(),
                        new JumpPlayerImpl());

            } else if (entity is IRobot) {


                this.skinStrategy = new SkinStrategy<X>(new IdleRobotImpl(),
                      new JumpRobotImpl());

            } else {
                Console.WriteLine("Is not possible to draw this entity" + entity);
            }

            switch (a)
            {
                case AnimationState.State.IS_IDLE:

                    this.animated = this.skinStrategy.idle();
                    break;

                case AnimationState.State.IS_JUMPING:

                    this.animated = this.skinStrategy.jump();
                    break;

                default:
                    break;


            }
            return this.animated;

        }

    }
}
