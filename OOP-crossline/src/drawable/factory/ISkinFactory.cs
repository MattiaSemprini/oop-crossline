﻿using OOP_crossline.drawable.product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable
{
    interface ISkinFactory<X>
    {
        /**
     * <param name="entity">type of skin to draw. </param> 
     * <returns><seealso cref="ISkinProduct"/></returns>
     */
        ISkinProduct createSkin(X entity);

        /**
         *Set the product to draw before draw.
         *@param a {@link AnimationState} the state to change.
         *@param entity {@link X} type of skin to draw.
         *@return Skin to draw.
         */
        ISkinProduct createSkinWithState(X entity, AnimationState.State a);

    }
}
