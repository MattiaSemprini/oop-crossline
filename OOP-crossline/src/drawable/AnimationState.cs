﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable
{
    public static class AnimationState
    {
        public enum State{
            /**
            *
            */
            IS_IDLE,
            /**
            *
            */
            IS_JUMPING,
            /**
            *
            */
            IS_RUNING,
            /**
            *
            */
            IS_SHOOTING,
            /**
            *
            */
            AS_HIT,
            /**
            *
            */
            AS_POWER

        };

        /**
        * Set Speed of animation frame.
        */
        public static float SPEED = 0.3f;

        /**
         * To know position of package of file.
         * @return String of path resource
         */
        public static String getAtlas()
        {
            return "Sprites.atlas";
        }

    }
}
