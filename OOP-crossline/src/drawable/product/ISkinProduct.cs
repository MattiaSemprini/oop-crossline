﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable.product
{
    public interface ISkinProduct
    {
      
      /**
      * know the type of skin.
      * <returns> <seealso cref="AnimationState.State"/> of skin</returns>
      */
        AnimationState.State GetState();

        /**
        * set the type of skin.
        * <param name="a">animation type</param>
        */
        void SetState(AnimationState.State a);
    }
}
