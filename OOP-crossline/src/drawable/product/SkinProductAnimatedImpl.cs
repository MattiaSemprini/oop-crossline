﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable.product
{
    class SkinProductAnimatedImpl : ISkinProduct
    {
        AnimationState.State state;

        public AnimationState.State State { get => state; set => state = value; }

        public AnimationState.State GetState()
        {
            return state;
        }

        public void SetState(AnimationState.State a)
        {
            state = a;
        }
    }
}
