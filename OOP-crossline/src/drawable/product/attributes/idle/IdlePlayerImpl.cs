﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable.product.attributes.idle
{
    class IdlePlayerImpl : IIdle
    {
        private ISkinProduct animation;

    /**
     * Idle.
     */
    public IdlePlayerImpl()
        {
            this.animation = new SkinProductAnimatedImpl();
            this.animation.SetState(AnimationState.State.IS_IDLE);

        }
    public ISkinProduct idle()
        {
            this.animation.SetState(AnimationState.State.IS_IDLE);
            return animation;
        }
    }
}
