﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable.product.attributes.jump
{
    interface IJump
    {

    /**
    *
    * <returns>SKin Product in jumping state</returns> 
    */
        ISkinProduct jump();
    }
}
