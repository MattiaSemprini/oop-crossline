﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable.product.attributes.jump
{
    class JumpPlayerImpl : IJump
    {
        private ISkinProduct animation;

        public JumpPlayerImpl()
        {
            this.animation = new SkinProductAnimatedImpl();
            this.animation.SetState(AnimationState.State.IS_JUMPING);

        }
        public ISkinProduct jump()
        {
            this.animation.SetState(AnimationState.State.IS_JUMPING);
            return animation;
        }
    }
}
