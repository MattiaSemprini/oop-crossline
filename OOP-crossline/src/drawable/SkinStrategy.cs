﻿using OOP_crossline.drawable.product;
using OOP_crossline.drawable.product.attributes.idle;
using OOP_crossline.drawable.product.attributes.jump;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_crossline.drawable
{
    class SkinStrategy<X>
    {
        /**
        * Interface for every type of Actor.
        *
        */
        private IIdle idleAnimation;
        private IJump jumpAnimation;
        private ISkinProduct entity;

        /**
         * Skin would now who is, player or robot.
         *
         * <param name="idle">Idle concrete class</param>
         * <param name="jump">Jump concrete class</param>
         * 
         */
        public SkinStrategy(IIdle idle, IJump jump)
        {
            this.idleAnimation = idle;
            this.jumpAnimation = jump;
        }

        /**
         * this method will call every render time to set animationEntity with correct frame
         * that will draw in the view.
         * <returns>animation in idle state</returns>
         */
        public ISkinProduct idle()
        {
            entity = this.idleAnimation.idle();
            return entity;
        }

        /**
         * this method will call every time you would to set <seealso cref="SkinProductAnimatedImpl"/> with correct animation
         * that will draw in the view.
         * <returns><seealso cref="SkinProductAnimatedImpl"/> animation in jump state</returns>
         */
        public ISkinProduct jump()
        {
            entity = this.jumpAnimation.jump();
            return entity;
        }
    }
}
